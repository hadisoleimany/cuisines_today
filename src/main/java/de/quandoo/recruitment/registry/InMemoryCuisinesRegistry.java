package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.stream.Collectors;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
    /*
    1 we shouldn't check objects with == because we just check the address, we should use equal method
    2 every custom class should have own implementation of equal and hashcode
    3 we should change  our implementation for multi thread environment
    4 all test are passed? without any check assertion
    5 if we want to scale our program what will happen, we have just 3 kind of food (italian,french,german)?
    6 if we pass the parameter in method why we need to keep in other place out of the method stack : String cuisineName
    7 about we should divide our logic and our storage also we should keep data in database beside in memory, and for
     using in memory we can use redis as an in memory database

     */
//    private List italianCuisineCustomers = new LinkedList();
//    private List frenchCuisineCustomers = new LinkedList();
//    private List germanCuisineCustomers = new LinkedList();
    private Map<Cuisine, List<Customer>> cuisineCustomerMap = new HashMap<>();

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        if (userId == null || cuisine == null) {
            return;
        }
        if (cuisineCustomerMap.containsKey(cuisine)) {
            cuisineCustomerMap.get(cuisine).add(userId);
        } else {
            List<Customer> customerList = new ArrayList<>();
            customerList.add(userId);
            cuisineCustomerMap.put(cuisine, customerList);
        }
//        if (cuisine.getName().equalsIgnoreCase("italian")) {
//            italianCuisineCustomers.add(userId);
//        }
//        if (cuisine.getName().equalsIgnoreCase("french")) {
//            frenchCuisineCustomers.add(userId);
//        } else if (cuisine.getName().equalsIgnoreCase("german")) {
//            germanCuisineCustomers.add(userId);
//        }
//
//        System.err.println("Unknown cuisine, please reach johny@bookthattable.de to update the code");
    }

    // distinct customers
    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        validation(cuisine);
        return cuisineCustomerMap.get(cuisine).stream().distinct().collect(Collectors.toList());
//        if (cuisine.getName().equals("italian")) {
//            return italianCuisineCustomers;
//        }
//        if (cuisine.getName().equals("french")) {
//            return frenchCuisineCustomers;
//        } else if (cuisine.getName().equals("german")) {
//            return germanCuisineCustomers;
//        }
//        return null;
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        validation(customer);

        return cuisineCustomerMap.entrySet()
                .stream()
                .filter(c -> c.getValue().contains(customer))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
//        if (italianCuisineCustomers.contains(customer)) {
//            return Arrays.asList(new Cuisine("italian"));
//        }
//        if (frenchCuisineCustomers.contains(customer)) {
//            return Arrays.asList(new Cuisine("french"));
//        }
//        if (germanCuisineCustomers.contains(customer)) {
//            return Arrays.asList(new Cuisine("german"));
//        }
    }

    private void validation(Object obj) {
        if (cuisineCustomerMap.isEmpty() || obj == null) {
            throw new RuntimeException(" your parameter is null , Please check");
        }
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if (n == 0) {
            return Collections.emptyList();
        }
        Map<Integer,Cuisine> map=new HashMap<>();
        for (Map.Entry<Cuisine,List<Customer>> entry : cuisineCustomerMap.entrySet()) {
            map.put(entry.getValue().size(),entry.getKey());
        }
        List<Cuisine> collect = map.entrySet().stream().sorted(Map.Entry.<Integer, Cuisine>comparingByKey().reversed()).map(Map.Entry::getValue)
                .collect(Collectors.toList());

        return collect.stream().limit(n).collect(Collectors.toList());
    }
}
