package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));

        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        assertEquals(1, customerList.size());
    }

    @Test
    public void shouldWork2() {
        Customer customer = new Customer("1");
        Cuisine german = new Cuisine("german");
        cuisinesRegistry.register(customer, new Cuisine("french"));
        cuisinesRegistry.register(customer, german);
        cuisinesRegistry.register(customer, german);
        List<Customer> customerList = cuisinesRegistry.cuisineCustomers(german);
        Assert.assertEquals(1, customerList.size());

    }

    @Test(expected = RuntimeException.class)
    public void cuisineCustomersError() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        Customer customer = new Customer("1");
        Cuisine german = new Cuisine("german");
        cuisinesRegistry.register(customer, new Cuisine("french"));
        cuisinesRegistry.register(customer, german);
        cuisinesRegistry.register(customer, german);

        List<Cuisine> cuisines = cuisinesRegistry.customerCuisines(customer);
        Assert.assertEquals(2, cuisines.size());
    }

    @Test(expected = RuntimeException.class)
    public void customerCuisinesError() {
        cuisinesRegistry.customerCuisines(null);
    }

    //    @Test(expected = RuntimeException.class)
    @Test
    public void thisDoesntWorkYet() {
        Cuisine german = new Cuisine("german");
        Cuisine french = new Cuisine("french");
        Cuisine italian = new Cuisine("italian");

        Customer customer = new Customer("1");
        Customer customer1 = new Customer("2");
        Customer customer2 = new Customer("3");
        Customer customer3 = new Customer("4");

        Customer customer4 = new Customer("5");
        Customer customer5 = new Customer("6");
        cuisinesRegistry.register(customer, german);
        cuisinesRegistry.register(customer1, german);
        cuisinesRegistry.register(customer2, german);
        cuisinesRegistry.register(customer3, german);

        cuisinesRegistry.register(customer4, french);
        cuisinesRegistry.register(customer5, french);

        cuisinesRegistry.register(customer, italian);
        cuisinesRegistry.register(customer1, italian);
        cuisinesRegistry.register(customer2, italian);
        cuisinesRegistry.register(customer3, italian);
        cuisinesRegistry.register(customer4, italian);
        cuisinesRegistry.register(customer5, italian);

        int top = 1;
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(top);

        assertEquals(top, cuisines.size());
        assertEquals(italian, cuisines.get(0));
    }

    @Test
    public void topCuisinesError() {
        List<Cuisine> cuisines = cuisinesRegistry.topCuisines(0);
        assertEquals(Collections.emptyList(), cuisines);
    }

}